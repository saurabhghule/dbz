@extends('index')

@section('content')

    {{--THANK YOU PAGE--}}
    <div class="thankYou-page">
        <div class="section-a valign-wrapper">
            <div class="row">
                <div class="columns large-12 small-12 medium-12">
                    <div class="section-content">
                        <div class="brand-logo text-center valign">
                            <img src="/images/logo.png" alt=""/>
                        </div>

                        <div class="punch-line text-center valign margin-top-50">
                            <h2 class="no-margin">
                                Thank You So Much For Connecting With Us <br/>
                                We Will Get Back To You In No Time
                            </h2>
                        </div>

                        <div class="site-link text-center valign margin-top-50">
                            <h5 class="no-margin">www.divbyz.com</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection