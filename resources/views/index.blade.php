<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="_route" content="{{Route::currentRouteName()}}"/>

    <title>Divide By Zero</title>

    <link rel="apple-touch-icon" sizes="57x57" href="/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png">
    <link rel="manifest" href="/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

	<link href="/css/app.css" rel="stylesheet">

	<!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Josefin+Sans:600,700' rel='stylesheet' type='text/css'>

</head>
<body>


@yield('content')


<!-- Scripts -->
<script type="text/javascript" src="/js/all.js"></script>

<div id="myModal" class="reveal-modal bgColor-secondaryBlack enquiry-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
    <div class="sign-up-form padding-tb-15">

        <div class="form-header margin-top-10">
            <h4 class="white-color text-right no-margin">Sign Up for your DBZ account</h4>
            <h6 class="dark-grey-color text-right margin-top-20">You can test everything out and make sure you are happy before paying us a dime.</h6>
        </div>

        <form class="margin-top-20">
            <div class="row">
                <div class="columns small-12 large-12 no-padding">
                    <div class="row collapse">
                        <div class="columns large-1 small-2">
                            <span class="prefix bgColor-yellow no-border border-radius-tl border-radius-bl"><i class="white-color fa fa-user"></i></span>
                        </div>
                        <div class="columns large-11 small-10">
                            <input tabindex="1" type="text" placeholder="Name" required pattern="[a-zA-Z]+" class="no-border border-radius-tr border-radius-br"/>
                            {{--<small class="error bgColor-black">Name is required and must be a string.</small>--}}
                        </div>
                    </div>
                </div>
                <div class="columns small-12 large-12 no-padding">
                    <div class="row collapse">
                        <div class="columns large-1 small-2">
                            <span class="prefix bgColor-yellow no-border border-radius-tl border-radius-bl"><i class="white-color fa fa-envelope-o"></i></span>
                        </div>
                        <div class="columns large-11 small-10">
                            <input tabindex="2" type="email" placeholder="Email" required class="no-border border-radius-tr border-radius-br"/>
                            {{--<small class="error bgColor-black">An email address is required.</small>--}}
                        </div>
                    </div>
                </div>
                <div class="columns small-12 large-12 no-padding">
                    <div class="row collapse">
                        <div class="columns large-1 small-2">
                            <span class="prefix bgColor-yellow no-border border-radius-tl border-radius-bl"><i class="white-color fa fa-phone-square"></i></span>
                        </div>
                        <div class="columns large-11 small-10">
                            <input tabindex="3" type="tel" placeholder="Phone" class="no-border border-radius-tr border-radius-br" />
                        </div>
                    </div>
                </div>
                <div class="columns small-12 large-12 no-padding">
                    <div class="row collapse">
                        <div class="columns large-1 small-2">
                            <span class="prefix bgColor-yellow no-border border-radius-tl border-radius-bl"><i class="white-color fa fa-comment"></i></span>
                        </div>
                        <div class="columns large-11 small-10">
                            <textarea tabindex="4" rows="3" placeholder="Comments" class="no-border border-radius-tr border-radius-br"></textarea>
                        </div>
                    </div>
                </div>

                <div class="columns small-offset-6 small-6 large-offset-9 large-3 end">
                    <a href="{{route('thankyou')}}" tabindex="5" type="submit" class="submit-btn white-color text-center no-margin bgColor-yellow border-radius-3">Submit</a>
                </div>
            </div>
        </form>
    </div>
    <a tabindex="6" class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>


</body>
</html>
