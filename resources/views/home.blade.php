@extends('index')


@section('content')

    <div class="landing-page">

        {{--Header--}}
        <div class="divByZ-header bgColor-black padding-tb-15">
            <div class="row">
                <div class="columns large-1 small-4 medium-3">
                    <div class="brand-logo">
                        <img src="/images/logo.png" alt=""/>
                    </div>
                </div>
                <div class="columns large-9 small-8 medium-9">
                    <div class="head-title">
                        <h5 class="text-center no-margin dark-grey-color small-only-font-16">CT Scans can be converted to 3D prints for preop planning & mock surgery.</h5>
                    </div>
                </div>
                <div class="columns large-2 small-8 medium-9">
                    <div class="enquiry-btn right">
                        <a data-reveal-id="myModal" class="no-margin white-color font-20">
                            <i class="fa fa-clipboard padding-r-15"></i>Enquiry Form
                        </a>
                    </div>
                </div>
            </div>
        </div>



        {{--Body--}}
        <section class="section section-1 valign-wrapper padding-t-75">
            <div class="section-content valign padding-tb-60">
                <div class="row">
                    <div class="columns small-offset-2 small-10 medium-offset-4 medium-8 large-5 large-offset-7">
                        <div class="sign-up-form">

                            <div class="form-header">
                                <h4 class="white-color text-right no-margin">Need a Quote</h4>
                                <h6 class="dark-grey-color text-right margin-top-20">Drop us a line and we will get back to you within 24 hours.</h6>
                            </div>

                            <form class="margin-top-20">
                                <div class="row">
                                    <div class="columns small-12 large-12">
                                        <div class="row collapse">
                                            <div class="columns large-1 small-2">
                                                <span class="prefix bgColor-yellow no-border border-radius-tl border-radius-bl"><i class="white-color fa fa-user"></i></span>
                                            </div>
                                            <div class="columns large-11 small-10">
                                                <input type="text" placeholder="Name" required pattern="[a-zA-Z]+" class="no-border border-radius-tr border-radius-br"/>
                                                {{--<small class="error bgColor-black">Name is required and must be a string.</small>--}}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="columns small-12 large-12">
                                        <div class="row collapse">
                                            <div class="columns large-1 small-2">
                                                <span class="prefix bgColor-yellow no-border border-radius-tl border-radius-bl"><i class="white-color fa fa-envelope-o"></i></span>
                                            </div>
                                            <div class="columns large-11 small-10">
                                                <input type="email" placeholder="Email" required class="no-border border-radius-tr border-radius-br"/>
                                                {{--<small class="error bgColor-black">An email address is required.</small>--}}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="columns small-12 large-12">
                                        <div class="row collapse">
                                            <div class="columns large-1 small-2">
                                                <span class="prefix bgColor-yellow no-border border-radius-tl border-radius-bl"><i class="white-color fa fa-phone-square"></i></span>
                                            </div>
                                            <div class="columns large-11 small-10">
                                                <input type="tel" placeholder="Phone" class="no-border border-radius-tr border-radius-br" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="columns small-12 large-12">
                                        <div class="row collapse">
                                            <div class="columns large-1 small-2">
                                                <span class="prefix bgColor-yellow no-border border-radius-tl border-radius-bl"><i class="white-color fa fa-comment"></i></span>
                                            </div>
                                            <div class="columns large-11 small-10">
                                                <textarea rows="3" placeholder="Comments" class="no-border border-radius-tr border-radius-br"></textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="columns small-offset-6 small-6 medium-offset-7 medium-5 large-offset-9 large-3 end">
                                        <a href="{{route('thankyou')}}" type="submit" class="submit-btn white-color text-center no-margin bgColor-yellow border-radius-3">Submit</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section-2">
            <div class="section-content">
                <div class="row">
                    <div class="columns large-12 small-12">
                        <div class="wrapper padding-tb-40">
                            <h4>Divide By Zero's Contribution in Ortho Surgery?</h4>

                            <ul class="margin-top-20 list-items fa-ul">
                                <li class="margin-top-20">
                                    <i class="fa fa-arrow-circle-o-right fa-li"></i>
                                    <h6 class="">A Team of doctors from Jawaharlal Institute of Postgraduate Medical Education and Reasearch (JIPMER) carried out very rare Ortho surgery</h6>
                                </li>

                                <li class="margin-top-20">
                                    <i class="fa fa-arrow-circle-o-right fa-li"></i>
                                    <h6 class="">CT Scan was converted in 3D file</h6>
                                </li>

                                <li class="margin-top-20">
                                    <i class="fa fa-arrow-circle-o-right fa-li"></i>
                                    <h6 class="">Prototype of patients skull was created using Divide By Zero Technologies printers, which played vital role in succes of Ortho surgery.</h6>
                                </li>
                            </ul>

                            <a data-reveal-id="myModal" class="bgColor-yellow white-color know-more-btn margin-top-10 border-radius-3">Get Case Study</a>
                        </div>
                    </div>
                </div>

                <div class="bgColor-grey">
                    <div class="row">
                        <div class="columns large-12 small-12 medium-12">
                            <div class="testimonial-slider">
                                <div class="testimonial-slide padding-40 small-only-padding-lr-20">
                                    <div class="slider-img padding-20 bgColor-white">
                                        <img src="/images/slide1.jpg" alt=""/>
                                    </div>
                                </div>
                                <div class="testimonial-slide padding-40 small-only-padding-lr-20">
                                    <div class="slider-img padding-20 bgColor-white">
                                        <img src="/images/slide2.jpg" alt=""/>
                                    </div>
                                </div>
                                <div class="testimonial-slide padding-40 small-only-padding-lr-20">
                                    <div class="slider-img padding-20 bgColor-white">
                                        <img src="/images/slide3.jpg" alt=""/>
                                    </div>
                                </div>

                                <div class="testimonial-slide padding-40 small-only-padding-lr-20">
                                    <div class="slider-img padding-20 bgColor-white">
                                        <img src="/images/slide4.jpg" alt=""/>
                                    </div>
                                </div>
                                <div class="testimonial-slide padding-40 small-only-padding-lr-20">
                                    <div class="slider-img padding-20 bgColor-white">
                                        <img src="/images/slide5.jpg" alt=""/>
                                    </div>
                                </div>
                                <div class="testimonial-slide padding-40 small-only-padding-lr-20">
                                    <div class="slider-img padding-20 bgColor-white">
                                        <img src="/images/slide6.jpg" alt=""/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section section-3 valign-wrapper">
            <div class="section-content valign padding-tb-40">
                <div class="row">
                    <div class="columns large-12">
                        <div class="content-header">
                        <span class="left padding-r-15">
                            <h2 class="no-margin">AION 500</h2>
                        </span>
                        <span class="left">
                            <h6 class="no-margin">
                                Creating a rage in 3D Printing Industry <br/> All across the globe.
                            </h6>
                        </span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="columns large-12">
                        <div class="content-sub-header margin-top-20">
                            <h5 class="no-margin">Features of AION 500</h5>
                        </div>
                    </div>
                </div>

                <div class="content-grid margin-top-20">
                    <div class="row">
                        <div class="columns large-7 small-12 medium-12">
                            <ul class="small-block-grid-1 medium-block-grid-1 large-block-grid-2 margin-left-1rem fa-ul">
                                <li class="padding-r-4em">
                                    <i class="fa fa-arrow-circle-o-right fa-li"></i>
                                    <h5 class="no-margin">Massive Build Volume </h5>
                                    <p class="no-margin-bottom margin-top-10">Now print as big as spinal cord CT scan file into 3D model in one go.</p>
                                </li>
                                <li class="padding-r-4em">
                                    <i class="fa fa-arrow-circle-o-right fa-li"></i>
                                    <h5 class="no-margin">Lean and Easy Workflow </h5>
                                    <p class="no-margin-bottom margin-top-10">Get Dicom Converted into 3D model without any hassle.</p>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="row">
                        <div class="columns large-7 small-12 medium-12">
                            <ul class="small-block-grid-1 large-block-grid-2 medium-block-grid-1 margin-left-1rem fa-ul">
                                <li class="padding-r-4em">
                                    <i class="fa fa-arrow-circle-o-right fa-li"></i>
                                    <h5 class="no-margin">Dual Nozzle</h5>
                                    <p class="no-margin-bottom margin-top-10">Now print complex medical models with zero post processing hassle.</p>
                                </li>
                                <li class="padding-r-4em">
                                    <i class="fa fa-arrow-circle-o-right fa-li"></i>
                                    <h5 class="no-margin">Multi Material 3D Printer</h5>
                                    <p class="no-margin-bottom margin-top-10">Now print with food grade Nylon, PLA, PETG, PC for medical application</p>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="row">
                        <div class="columns large-7 small-12 medium-12">
                            <ul class="small-block-grid-1 large-block-grid-2 medium-block-grid-1 margin-left-1rem fa-ul">
                                <li class="padding-r-4em">
                                    <i class="fa fa-arrow-circle-o-right fa-li"></i>
                                    <h5 class="no-margin">Unmatched Precision</h5>
                                    <p class="no-margin-bottom margin-top-10">Print medical models within 50 micron accuracy</p>
                                </li>
                                <li class="padding-r-4em">
                                    <i class="fa fa-arrow-circle-o-right fa-li"></i>
                                    <h5 class="no-margin">Affordable & Economical</h5>
                                    <h6 class="margin-top-10 font-18">Low printing cost @ INR 4 per gram</h6>
                                    <a href="{{route('thankyou')}}" data-reveal-id="myModal" class="bgColor-yellow white-color know-more-btn margin-top-10 border-radius-3">Know More</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section-4 valign-wrapper">
            <div class="section-content valign padding-tb-90 small-only-padding-tb-40">
                <div class="row">
                    <div class="columns large-12">

                        <div class="content-header">
                            <h4>About DIV BY ZERO Technologies</h4>
                        </div>

                        <div class="content-body">
                            Div By Zero Technologies believe that when the technology becomes accessible and affordable,
                            then a path of economic and creative development is unleashed.The sole purpose of div by zero
                            technologies is to innovate and introduce indigenous technologies for creating value for
                            business markets all across the globe.
                        </div>

                    </div>
                </div>
            </div>
        </section>


        {{--Footer--}}
        <div class="footer bgColor-black">
            <div class="row">
                <div class="columns large-12 small-12 medium-12">
                    <div class="copy-note padding-tb-20">
                        <h6 class="white-color no-margin">All Rights Reserved @ DivByZ</h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

