
var _route = '';

$(document).ready(function(){

    _route = $('meta[name=_route]').attr('content');

    $(document).foundation();

    if(_route == 'home'){
        $('.testimonial-slider').slick({
            easing:'linear',
            arrows: true,
            dots: false,
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
            cssEase: 'linear',
            pauseOnHover: false,
            prevArrow: '<span class="nav-prev slick-arrow "><i class="fa fa-chevron-left"></i></span>',
            nextArrow: '<span class="nav-next slick-arrow "><i class="fa fa-chevron-right"></i></span>',

            responsive: [
                {
                    breakpoint: 1023,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 640,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        swipe:true,
                        touchMove:true,
                        draggable:true,
                        fade:true
                    }
                }
            ]
        });
    }

    $(window).on('scroll',function(){
        if($('.section-2 .wrapper').visible()){
            $('.divByZ-header').find('.head-title').addClass('hide');
            $('.divByZ-header').find('.head-title').removeClass('visible');
            $('.divByZ-header').find('.enquiry-btn').addClass('visible');
            $('.divByZ-header').find('.enquiry-btn').removeClass('hide');
        }
        if($(window).scrollTop() == 0){
            $('.divByZ-header').find('.head-title').removeClass('hide');
            $('.divByZ-header').find('.enquiry-btn').removeClass('visible');
            $('.divByZ-header').find('.head-title').addClass('visible');
            $('.divByZ-header').find('.enquiry-btn').addClass('hide');
        }
    });

});









