var elixir = require('laravel-elixir');

elixir.config.sourcemaps = true;

elixir(function (mix) {
    mix.sass('app.scss')
        .scripts([
            'vendor/jquery.js',
            'vendor/modernizr.js',
            'vendor/foundation.min.js',
            'vendor/slick.min.js',
            'vendor/jquery.visible.min.js',
            'script.js'
        ],'public/js/all.js', 'resources/assets/js');
});
